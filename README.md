
# Book store service
> Book store is designed on Symphony 4 
This application has  end points :
GET: This is used for getting the book based in id from database. 
 http://127.0.0.1:8000/bookstore/30
 
POST: For adding the book in the data base.
http://127.0.0.1:8000/bookstore

Sample request :  
{
        "title": "docker1",
        "isbn": "isbn19",
   		"addedOn": "2018-05-08T12:30:50"
  }

## Installing / Getting started

A quick introduction of the minimal setup you need to get a Book store  up & running.

## Developing

### Built With
PHP ,Symphony 4 
Composer for managing dependencies 
Doctrine as ORM 


### Setting up Dev

Here's a brief intro about what a developer must do in order to start developing
the project further:

```shell
#Clone
git clone https://gitlab.com/pathak.vishal2k8/bookstore-rest.git
cd bookstore-rest
#Run commandph
php bin/console server:run

#Goto any browser:
#Type URL :
GET: This is used for getting the book based in id from database. 
 http://127.0.0.1:8000/bookstore/30
 
POST: For adding the book in the data base.
http://127.0.0.1:8000/bookstore
 
 				
```


### Building

#Docker
Docker is not implemented here  