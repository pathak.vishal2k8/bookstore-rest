<?php

namespace App\Controller;

use App\Entity\Book;
use App\Form\BookType;
use App\Repository\BookRepository;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManagerInterface;

use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Psr\Log\LoggerInterface;


/**
 * @Rest\RouteResource("bookstore",  pluralize=false)
 */
class BookStoreController extends FOSRestController implements ClassResourceInterface{

    private $logger;

    /**
     * @var EntityManagerInterface
     */
private $entityManager;

    /**
     * @var BookRepository
     */
    private $bookRepository;

  

    public function __construct( EntityManagerInterface $entityManager, BookRepository $bookRepository,
                         LoggerInterface $logger    
    ) {
        $this->entityManager = $entityManager;
        $this->bookRepository     = $bookRepository;
        $this->logger = $logger;
    }

  
    public function postAction( Request $request  ) {
        $form = $this->createForm(BookType::class, new Book());
        $this->logger->debug('Some debug');
        $this->logger->info('Some info');
        $form->submit($request->request->all());

        if (false === $form->isValid()) {

            return $this->handleView(
                $this->view($form)
            );
        }

        $this->entityManager->persist($form->getData());
        $this->entityManager->flush();

        return $this->handleView(
            $this->view(
                [
                    'status' => 'ok',
                    'message' => 'Data saved succesfully'

                ],
                Response::HTTP_CREATED
            )
        );
    }

  
    public function getAction( $id )  {
     
        $this->logger->debug('Get method called ....');
        $this->logger->info('Some info about get');
       
        return new JsonResponse(   $this->findBookById($id), JsonResponse::HTTP_OK );
        
    }

    /**
     * @param $id
     *
     * @return Book|null
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function findBookById($id)
    {
        $book = $this->bookRepository->find($id);

        if (null === $book) {
            $this->logger->error('An error occurred NotFoundHttpException id is not found in DB....');
            throw new NotFoundHttpException();
        }

        return $book;
    }
     
}
