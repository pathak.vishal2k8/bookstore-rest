<?php

// src/Entity/Book.php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookRepository")
 */
class Book implements  \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * 
     * @ORM\column(type="string")
     */
    private $title;
    /**
     * 
     * @ORM\column(type="string")
     */
    private $isbn;

/**
     * @var \DateTime|null
     * @ORM\column(type="datetime")
     */
    private $addedOn;


    /**
     * @return \DateTime|null
     */
    public function getAddedOn(): ?\DateTime
    {
        return $this->addedOn;
    }

    /**
     * @param \DateTime $addedOn
     *
     * @return Book
     */
    public function setaddedOn($addedOn): Book
    {
        $this->addedOn = $addedOn;

        return $this;
    }


    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Book
     */
    public function setTitle($title): Book
    {
        $this->title = $title;

        return $this;
    }


    /**
     * @return string|null
     */
    public function getisbn(): ?string
    {
        return $this->isbn;
    }

    /**
     * @param string $isbn
     *
     * @return Book
     */
    public function setisbn($isbn): Book
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id'           => $this->id,
            'title'        => $this->title,
            'addedOn'      => $this->addedOn,
            'isbn'         => $this->isbn,
        ];
    }
}