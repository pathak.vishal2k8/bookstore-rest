<?php

// src/Form/BookType.php

namespace App\Form;

use App\Entity\Book;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('addedOn',
                 DateTimeType::class,
                 [
                     'widget'        => 'single_text',
                    // 'format'        => 'yyyy-MM-dd',
                     'format'        => 'yyyy-MM-dd\'T\'HH:mm:ss',
                     'property_path' => 'addedOn',
                 ]
             )
            ->add(
                'isbn',
                TextType::class,
                [
                    'property_path' => 'isbn',
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Book::class,
                'allow_extra_fields' => true
             // 'csrf_protection' => false,
            ]
        );
    }
}